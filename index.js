'use strict';
const emailRegex = require('email-regex');
const uuidValidate = require('uuid-validate');
const _ = require('lodash');
const isEmpty = function(value) {
  return value === undefined || value === null || value === '';
};

function overrideErrorMessage(func, message) {
  return function() {
    if( func.apply(null, arguments) !== undefined ) {
      return message;
    }
  }
}

function required(value) {
  if (isEmpty(value)) {
    return 'Required';
  }
}

function email(value) {
  if(required(value) === 'Required') { return; }
  // This is used to see if email fields are "email like", it's not perfect.
  // See this: https://davidcel.is/posts/stop-validating-email-addresses-with-regex/
  if (!emailRegex({
      exact: true
    }).test(value)) {
    return 'Invalid email address';
  }
}

function isString(value) {
  if(required(value) === 'Required') { return; }
  if (!_.isString(value)) {
    return 'Must be a string';
  }
}

function integer(value) {
  if(required(value) === 'Required') { return; }
  if (!_.isInteger(value)) {
    return 'Must be an integer';
  }
}

function uuid(value) {
  if(required(value) === 'Required') { return; }
  if (!uuidValidate(value, 4)) {
    return 'Must be a uuid';
  }
}

function match(field) {
  return function(value, data) {
    if(required(value) === 'Required') { return; }
    if (value !== data[field]) {
      return 'Do not match';
    }
  };
}

function atLeast(min) {
  return function(value) {
    if(required(value) === 'Required') { return; }
    if (value < min) {
      return 'Must be at least ' + min;
    }
  }
}

//only for strings
function minLength(min) {
  return function(value) {
    if(required(value) === 'Required') { return; }
    if (typeof value !== 'string' || value.length < min) {
      return 'Must be at least ' + min + ' characters';
    }
  };
}

function minLengthArray(min) {
  return function (value) {
    if(required(value) === 'Required') { return; }
    if (!_.isArray(value) || value.length < min) {
      return 'Must be an array of at least length ' + min
    }
  }
}

//only for strings
function maxLength(max) {
  return function(value) {
    if(required(value) === 'Required') { return; }
    if (!isEmpty(value) && (typeof value !== 'string' || value.length > max)) {
      return 'Must be no more than ' + max + ' characters';
    }
  };
}

function oneOf(enumeration, projection) {
  return function(value) {
    if(required(value) === 'Required') { return; }
    if (!_.isArray(enumeration)) {
      throw new Error('Enumeration must be an array');
    }
    var realEnumeration;
    if (projection) {
      realEnumeration = enumeration.map(projection);
    } else {
      realEnumeration = enumeration;
    }
    if (!realEnumeration.includes(value)) {
      return 'Must be one of: ' + realEnumeration.join(', ') + '.';
    }
  };
}

function notOneOf(enumeration, projection) {
  return function(value) {
    if(required(value) === 'Required') { return; }
    if (!_.isArray(enumeration)) {
      throw new Error('Enumeration must be an array');
    }
    var realEnumeration;
    if (projection) {
      realEnumeration = enumeration.map(projection);
    } else {
      realEnumeration = enumeration;
    }
    if (realEnumeration.includes(value)) {
      return 'Must not be one of: ' + realEnumeration.join(', ') + '.';
    }
  };
}

function contains(value) {
  return function (array) {
    if (!_.isArray(array)) {
      throw new Error('Provided array must be an array');
    }
    if (!array.includes(value)) {
      return 'Must contain: ' + value + '.';
    }
  }
}

function unique(value, data, key, collection) {
  if(required(value) === 'Required') { return; }
  const duplicate = collection
    .map(function(item) {return item[key]})
    .filter(function(val) { return val === value });
  if (duplicate.length > 0) {
    return '' + key + ' must be unique. That one is already taken.';
  }
}

function valueExistsInCollection(value, data, key, collection) {
  if(required(value) === 'Required') { return; }
  const duplicate = collection
    .map(function(item) { return item[key] })
    .filter(function(val) { return val === value });
  if (duplicate.length === 0) {
    return '' + key + ' must exist with value ' + value + '.';
  }
}
// rules is an array of functions.
function join(rules) {
  return function(value, data, key, collection) {
    return rules.map(function(rule) {
      return rule(value, data, key, collection)
    }).filter(function(error) {
      return !!error
    });
  }
}

// function createValidator(rules) {
//   return function(data, props, collection) {
//     if(collection === undefined) { collection = []; }
//     var getErrorsForRules = function(rules, data, collection) {
//       const errors = {};
//       Object.keys(rules).forEach(function(key) {
//         if (rules[key] instanceof Array) {
//           const rule = join([].concat(rules[key])); // concat enables both functions and arrays of functions
//           const error = rule(data[key], data, key, collection);
//           if (error) {
//             errors[key] = error;
//           }
//         } else {
//           if (data.hasOwnProperty(key)) {
//             var subErrors = getErrorsForRules(rules[key], data[key], collection.map(function(item) {
//               return item[key]
//             }));
//             if (Object.keys(subErrors).length > 0) {
//               errors[key] = subErrors;
//             }
//           } else {
//             throw new Error('Data ' + JSON.stringify(data) + ' to be validated is missing key ' + key);
//           }
//         }
//       });
//       return errors;
//     }
//     const errors = getErrorsForRules(rules, data, collection);
//     return errors;
//   };
// }


var getErrorsForRules = function(rules, data, collection) {
  const errors = {};
  Object.keys(rules).forEach(function(key) {
    if (rules[key] instanceof Array) {
      const rule = join([].concat(rules[key])); // concat enables both functions and arrays of functions
      const keyErrors = rule(data[key], data, key, collection);
      if (keyErrors.length > 0) {
        errors[key] = keyErrors;
      }
    } else {
      if (data.hasOwnProperty(key)) {
        var subErrors = getErrorsForRules(rules[key], data[key], collection.map(function(item) {
          return item[key];
        }));
        if (Object.keys(subErrors).length > 0) {
          errors[key] = subErrors;
        }
      } else {
        throw new Error('Data ' + JSON.stringify(data) + ' to be validated is missing key ' + key);
      }
    }
  });
  return errors;
}

var getErrorsForForeignKeys = function (foreignKeys, data, collections) {
  var isForeignKey = function (object) {
    var correctKeyCount = Object.keys(object).length === 2;
    var correctKeyNames = object.primary && object.collection;
    return correctKeyCount && correctKeyNames;
  }
  var foreignKeyErrors = {};
  Object.keys(foreignKeys).forEach(function(key) {
    var keyObject = foreignKeys[key];
    if (isForeignKey(keyObject)) {
      //this allows for optional foreign keys
      //if a foreign key is required, make sure and include the 'required' validation
      //on the validation object
      //this will only validate foreign keys if they exist
      if (data[key] !== undefined) {
        var primaryKey = keyObject.primary;
        var collection = collections[keyObject.collection];
        var validationObject = {};
        validationObject[primaryKey] = [valueExistsInCollection];
        var validateKey = createValidator(validationObject);
        var keyDataObject = {};
        keyDataObject[primaryKey] = data[key];
        var keyErrors = validateKey(keyDataObject, null, collection);
        if (Object.keys(keyErrors).length > 0) {
          foreignKeyErrors[key] = keyErrors[primaryKey];
        }
      }
    } else {
      var subErrors = getErrorsForForeignKeys(keyObject, data[key], collections);
      if (Object.keys(subErrors).length > 0) {
        foreignKeyErrors[key] = subErrors;
      }
    }
  });
  return foreignKeyErrors;
}

var mergeErrors = function (errorObjectOne, errorObjectTwo) {
  var getMergedPropertyValue = function (key, objectOne, objectTwo) {
    var result;
    var value = objectOne[key];
    if (objectTwo.hasOwnProperty(key)) {
      if (typeof(value) === 'object' && !_.isArray(value)) {
          result = mergeErrors(objectOne[key], objectTwo[key]);
      } else {
        result = objectOne[key].concat(objectTwo[key]);
      }
    } else {
      result = value;
    }

    return result;
  }
  var merged = {};
  for (var key in errorObjectOne) {
    merged[key] = getMergedPropertyValue(key, errorObjectOne, errorObjectTwo);
  };
  for (var key in errorObjectTwo) {
    merged[key] = getMergedPropertyValue(key, errorObjectTwo, errorObjectOne);
  }

  return merged;
};

function createValidator(rules, foreignKeys) {
  return function(data, props, collections) {
    foreignKeys = foreignKeys || {};
    if (collections instanceof Array) {
      collection = collections; //only one collection passed in
    } else { //root collection is the one used for top level data validation
      collections = collections || {};
      var collection = [];
      if (collections.hasOwnProperty('root')) {
        collection = collections.root;
      }
    }
    //determine top level data errors
    const rulesErrors = getErrorsForRules(rules, data, collection);
    const foreignKeyErrors = getErrorsForForeignKeys(foreignKeys, data, collections);
    //merge foreign key errors into rules errors
    const errors = mergeErrors(rulesErrors, foreignKeyErrors);
    return errors;
  };
}

exports = module.exports = {
  email: email,
  required: required,
  isString: isString,
  integer: integer,
  uuid: uuid,
  match: match,
  atLeast: atLeast,
  minLength: minLength,
  minLengthArray: minLengthArray,
  maxLength: maxLength,
  oneOf: oneOf,
  notOneOf: notOneOf,
  contains: contains,
  unique: unique,
  valueExistsInCollection: valueExistsInCollection,
  createValidator: createValidator,
  overrideErrorMessage: overrideErrorMessage
};
