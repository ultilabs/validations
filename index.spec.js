const { expect } = require('chai');
const { email, required, isString, integer, uuid, match, atLeast, minLength, minLengthArray, maxLength, oneOf, notOneOf, contains, unique, valueExistsInCollection, createValidator, overrideErrorMessage } = require('./');

describe('validations', () => {
  describe('required', () => {
    it('Given input is empty string, should return error message', () => {
      // Arrange
      const input = '';

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal('Required');
    });

    it('Given input is undefined, should return error message', () => {
      // Arrange
      const input = undefined;

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal('Required');
    });

    it('Given input is null, should return error message', () => {
      // Arrange
      const input = null;

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal('Required');
    });

    it('Given input is {}, should return undefined', () => {
      // Arrange
      const input = {};

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is [], should return undefined', () => {
      // Arrange
      const input = [];

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is a string of letters, should return undefined', () => {
      // Arrange
      const input = "abc";

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is a string of numbers, should return undefined', () => {
      // Arrange
      const input = "123";

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is a string of letters and numbers, should return undefined', () => {
      // Arrange
      const input = "abc123";

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is a true, should return undefined', () => {
      // Arrange
      const input = true;

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is a false, should return undefined', () => {
      // Arrange
      const input = false;

      // Act
      const output = required(input);

      // Assert
      expect(output).to.equal(undefined);
    });
  });
  describe('email', () => {
    it('Empty string should return undefined', () => {
      // Arrange
      const input = '';

      // Act
      const output = email(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Non empty string should return error message', () => {
      // Arrange
      const input = 'abc';

      // Act
      const output = email(input);

      // Assert
      expect(output).to.equal('Invalid email address');
    });

    it('Non empty string with @ symbol at end should return error message', () => {
      // Arrange
      const input = 'abc@';

      // Act
      const output = email(input);

      // Assert
      expect(output).to.equal('Invalid email address');
    });

    it('Non empty string with @ symbol in middle but no TLD should return error message', () => {
      // Arrange
      const input = 'abc@abc';

      // Act
      const output = email(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Non empty string with @ symbol in middle and ending in . should return error message', () => {
      // Arrange
      const input = 'abc@abc.';

      // Act
      const output = email(input);

      // Assert
      expect(output).to.equal('Invalid email address');
    });

    it('Non empty string with @ in middle with TLD should return undefined', () => {
      // Arrange
      const input = 'abc@abc.com';

      // Act
      const output = email(input);

      // Assert
      expect(output).to.equal(undefined);
    });
  });

  describe('isString', () => {
    it('Given input is undefined, should return undefined', () => {
      // Arrange
      const input = undefined;

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is null, should return undefined', () => {
      // Arrange
      const input = null;

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is an empty string, should return undefined', () => {
      // Arrange
      const input = '';

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is an empty object, should return an error message', () => {
      // Arrange
      const input = {};

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal('Must be a string');
    });

    it('Given input is an array of length 0, should return an error message', () => {
      // Arrange
      const input = [];

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal('Must be a string');
    });

    it('Given input is a number, should return an error message', () => {
      // Arrange
      const input = 314;

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal('Must be a string');
    });

    it('Given input is a boolean, should return an error message', () => {
      // Arrange
      const input = true;

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal('Must be a string');
    });

    it('Given input is a string of nonzero length, should return undefined', () => {
      // Arrange
      const input = '123abc';

      // Act
      const output = isString(input);

      // Assert
      expect(output).to.equal(undefined);
    });

  });

  describe('integer', () => {
    it('Given input is undefined, should return undefined', () => {
      // Arrange
      const input = undefined;

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is null, should return undefined', () => {
      // Arrange
      const input = null;

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is an empty string, should return undefined', () => {
      // Arrange
      const input = '';

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is an empty object, should return an error message', () => {
      // Arrange
      const input = {};

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal('Must be an integer');
    });

    it('Given input is an array of length 0, should return an error message', () => {
      // Arrange
      const input = [];

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal('Must be an integer');
    });

    it('Given input is an integer, should return undefined', () => {
      // Arrange
      const input = 314;

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is a decimal number, should return an error message', () => {
      // Arrange
      const input = 3.14

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal('Must be an integer');
    });


    it('Given input is a boolean, should return an error message', () => {
      // Arrange
      const input = true;

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal('Must be an integer');
    });

    it('Given input is a string of nonzero length, should return an error message', () => {
      // Arrange
      const input = '123abc';

      // Act
      const output = integer(input);

      // Assert
      expect(output).to.equal('Must be an integer');
    });

  });

  describe('uuid', () => {
    it('Given input is undefined, should return undefined', () => {
      // Arrange
      const input = undefined;

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is null, should return undefined', () => {
      // Arrange
      const input = null;

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is an empty string, should return undefined', () => {
      // Arrange
      const input = '';

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given input is an empty object, should return an error message', () => {
      // Arrange
      const input = {};

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal('Must be a uuid');
    });

    it('Given input is an array of length 0, should return an error message', () => {
      // Arrange
      const input = [];

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal('Must be a uuid');
    });

    it('Given input is a number, should return an error message', () => {
      // Arrange
      const input = 314;

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal('Must be a uuid');
    });

    it('Given input is a boolean, should return an error message', () => {
      // Arrange
      const input = true;

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal('Must be a uuid');
    });

    it('Given input is a string of nonzero length that is not a uuid, should return an error message', () => {
      // Arrange
      const input = 'mjfghcv5-nh0f-445f-bd47-b661f28c1e27';

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal('Must be a uuid');
    });

    it('Given input is a string of nonzero length that is a uuid but is not a v4 uuid, should return an error message', () => {
      // Arrange
      const input = 'cf76212c-3fa9-11e6-beb8-9e71128cae77';

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal('Must be a uuid');
    });

    it('Given input is a string of nonzero length that is a v4 uuid, should return undefined', () => {
      // Arrange
      const input = '7c514c22-afbb-44a3-9615-e384cc6ea2b6';

      // Act
      const output = uuid(input);

      // Assert
      expect(output).to.equal(undefined);
    });

  });

  describe('match', () => {
    it('Given value is undefined and it is matched with undefined, should return undefined', () => {
      // Arrange
      const value = undefined;
      const fieldName = 'test';
      const data = {};
      data[fieldName] = undefined;

      // Act
      const output = match(fieldName)(value, data);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is undefined and it is matched with null, should return undefined', () => {
      // Arrange
      const value = undefined;
      const fieldName = 'test';
      const data = {};
      data[fieldName] = null;

      // Act
      const output = match(fieldName)(value, data);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a number and it is matched with an identical number, should return undefined', () => {
      // Arrange
      const value = 314;
      const fieldName = 'test';
      const data = {};
      data[fieldName] = 314;

      // Act
      const output = match(fieldName)(value, data);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a number and it is matched with a different number, should return an error', () => {
      // Arrange
      const value = 314;
      const fieldName = 'test';
      const data = {};
      data[fieldName] = 628;

      // Act
      const output = match(fieldName)(value, data);

      // Assert
      expect(output).to.equal('Do not match');
    });

    it('Given value is a boolean and it is matched with a string, should return an error', () => {
      // Arrange
      const value = true;
      const fieldName = 'test';
      const data = {};
      data[fieldName] = 'true';

      // Act
      const output = match(fieldName)(value, data);

      // Assert
      expect(output).to.equal('Do not match');
    });

    it('Given value is a boolean and it is matched with an identical boolean, should return undefined', () => {
      // Arrange
      const value = true;
      const fieldName = 'test';
      const data = {};
      data[fieldName] = true;

      // Act
      const output = match(fieldName)(value, data);

      // Assert
      expect(output).to.equal(undefined);
    });
  });

  describe('atLeast', () => {
    it('Given value is a string and it is compared to a string that is lexiographically after it, it should return an error message', () => {
      // Arrange
      const value = 'aaa';
      const min = 'zzz';

      // Act
      const output = atLeast(min)(value);

      // Assert
      expect(output).to.equal('Must be at least zzz');
    });

    it('Given value is a string and it is compared to a string that is lexiographically before it, it should return undefined', () => {
      // Arrange
      const value = 'zzz';
      const min = 'aaa';

      // Act
      const output = atLeast(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a number and it is compared to a number that is numerically after it, it should return an error message', () => {
      // Arrange
      const value = 7;
      const min = 10.14;

      // Act
      const output = atLeast(min)(value);

      // Assert
      expect(output).to.equal('Must be at least 10.14');
    });

    it('Given value is a number and it is compared to a number that is numerically before it, it should return undefined', () => {
      // Arrange
      const value = 16.3;
      const min = 9;

      // Act
      const output = atLeast(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });
  });

  describe('minLength', () => {
    it('Given value is undefined and min is 0, it should return undefined', () => {
      // Arrange
      const value = undefined;
      const min = 0;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is null and min is 0, it should return undefined', () => {
      // Arrange
      const value = null;
      const min = 0;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an empty string and min is 0, it should return undefined', () => {
      // Arrange
      const value = '';
      const min = 0;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });
    it('Given value is an empty string and min is 1, it should return undefined', () => {
      // Arrange
      const value = '';
      const min = 1;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string of nonzero length and min is an integer greater than the length of the string, it should return an error message', () => {
      // Arrange
      const value = 'test';
      const min = 5;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal('Must be at least 5 characters');
    });

    it('Given value is a string of nonzero length and min is an integer equal to the length of the string, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const min = 4;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string of nonzero length and min is an integer less than the length of the string, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const min = 3;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an array of nonzero length and min is an integer less than the length of the array, it should return an error message', () => {
      // Arrange
      const value = ['t', 'e', 's', 't'];
      const min = 3;

      // Act
      const output = minLength(min)(value);

      // Assert
      expect(output).to.equal('Must be at least 3 characters');
    });
  });

  describe('minLengthArray', () => {
    it('Given value is undefined and min is 0, it should return undefined', () => {
      // Arrange
      const value = undefined;
      const min = 0;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is null and min is 0, it should return undefined', () => {
      // Arrange
      const value = null;
      const min = 0;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an empty string and min is 0, it should return undefined', () => {
      // Arrange
      const value = '';
      const min = 0;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });
    it('Given value is an empty string and min is 1, it should return undefined', () => {
      // Arrange
      const value = '';
      const min = 1;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an empty array and min is 0, it should return undefined', () => {
      // Arrange
      const value = [];
      const min = 0;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an array of nonzero length and min is an integer greater than the length of the array, it should return an error message', () => {
      // Arrange
      const value = ['t', 'e', 's', 't'];
      const min = 5;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal('Must be an array of at least length 5');
    });

    it('Given value is an array of nonzero length and min is an integer equal to the length of the array, it should return undefined', () => {
      // Arrange
      const value = ['t', 'e', 's', 't'];
      const min = 4;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an array of nonzero length and min is an integer less than the length of the array, it should return undefined', () => {
      // Arrange
      const value = ['t', 'e', 's', 't'];
      const min = 3;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string of nonzero length and min is an integer less than the length of the string, it should return an error message', () => {
      // Arrange
      const value = 'test';
      const min = 3;

      // Act
      const output = minLengthArray(min)(value);

      // Assert
      expect(output).to.equal('Must be an array of at least length 3');
    });
  });

  describe('maxLength', () => {
    it('Given value is undefined and max is 0, it should return undefined', () => {
      // Arrange
      const value = undefined;
      const max = 0;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is null and max is 0, it should return undefined', () => {
      // Arrange
      const value = null;
      const max = 0;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an empty string and max is 0, it should return undefined', () => {
      // Arrange
      const value = '';
      const max = 0;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal(undefined);
    });
    it('Given value is an empty string and max is 1, it should return undefined', () => {
      // Arrange
      const value = '';
      const max = 1;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string of nonzero length and max is an integer greater than the length of the string, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const max = 5;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string of nonzero length and max is an integer equal to the length of the string, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const max = 4;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string of nonzero length and max is an integer less than the length of the string, it should return an error message', () => {
      // Arrange
      const value = 'test';
      const max = 3;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal('Must be no more than 3 characters');
    });

    it('Given value is an array of nonzero length and max is an integer greater than the length of the array, it should return an error message', () => {
      // Arrange
      const value = ['t', 'e', 's', 't'];
      const max = 5;

      // Act
      const output = maxLength(max)(value);

      // Assert
      expect(output).to.equal('Must be no more than 5 characters');
    });
  });

  describe ('oneOf', () => {
    it('Given value is an integer and the enumeration contains that integer, it should return undefined', () => {
      // Arrange
      const value = 5;
      const enumeration = [1, 2, 3, 4 ,5];

      // Act
      const output = oneOf(enumeration)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is an integer and the enumeration does not contain that integer, it should return an error message', () => {
      // Arrange
      const value = 5;
      const enumeration = [1, 2, 3, 4];

      // Act
      const output = oneOf(enumeration)(value);

      // Assert
      expect(output).to.equal('Must be one of: 1, 2, 3, 4.');
    });

    it('Given value is a string and the enumeration contains that string, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const enumeration = ['this', 'is', 'a', 'test'];

      // Act
      const output = oneOf(enumeration)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string and the enumeration does not contain that string, it should return an error message', () => {
      // Arrange
      const value = 'test';
      const enumeration = ['this', 'is', 'an', 'array'];

      // Act
      const output = oneOf(enumeration)(value);

      // Assert
      expect(output).to.equal('Must be one of: this, is, an, array.');
    });

    it('Given value is a string and the enumeration does contain that string, but each entry also contains extra data, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const enumeration = [
        {
          key: 'hi',
          value: 'word'
        },
        {
          key: 'bye',
          value: 'test'
        },
        {
          key: 'last',
          value: 'done'
        }
      ];
      const map = function (entry) {
        return entry.value;
      }

      // Act
      const output = oneOf(enumeration, map)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string and the enumeration does not contain that string, but each entry also contains extra data, it should return an error message', () => {
      // Arrange
      const value = 'test';
      const enumeration = [
        {
          key: 'hi',
          value: 'word'
        },
        {
          key: 'bye',
          value: 'nope'
        },
        {
          key: 'last',
          value: 'done'
        }
      ];
      const map = function (entry) {
        return entry.value;
      }

      // Act
      const output = oneOf(enumeration, map)(value);

      // Assert
      expect(output).to.equal('Must be one of: word, nope, done.');
    });

    it('Given the enumeration is not an array, it should throw an exception', () => {
      // Arrange
      const value = 'test';
      const enumeration = 'string';
      const testFunction = () => {
        oneOf(enumeration)(value);
      };

      // Act


      // Assert
      expect(testFunction).to.throw('Enumeration must be an array');
    });

  });

  describe ('notOneOf', () => {
    it('Given value is an integer and the enumeration contains that integer, it should return an error message', () => {
      // Arrange
      const value = 5;
      const enumeration = [1, 2, 3, 4 ,5];

      // Act
      const output = notOneOf(enumeration)(value);

      // Assert
      expect(output).to.equal('Must not be one of: 1, 2, 3, 4, 5.');
    });

    it('Given value is an integer and the enumeration does not contain that integer, it should return undefined', () => {
      // Arrange
      const value = 5;
      const enumeration = [1, 2, 3, 4];

      // Act
      const output = notOneOf(enumeration)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string and the enumeration contains that string, it should return an error message', () => {
      // Arrange
      const value = 'test';
      const enumeration = ['this', 'is', 'a', 'test'];

      // Act
      const output = notOneOf(enumeration)(value);

      // Assert
      expect(output).to.equal('Must not be one of: this, is, a, test.');
    });

    it('Given value is a string and the enumeration does not contain that string, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const enumeration = ['this', 'is', 'an', 'array'];

      // Act
      const output = notOneOf(enumeration)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string and the enumeration does contain that string, but each entry also contains extra data, it should return an error message', () => {
      // Arrange
      const value = 'test';
      const enumeration = [
        {
          key: 'hi',
          value: 'word'
        },
        {
          key: 'bye',
          value: 'test'
        },
        {
          key: 'last',
          value: 'done'
        }
      ];
      const map = function (entry) {
        return entry.value;
      }

      // Act
      const output = notOneOf(enumeration, map)(value);

      // Assert
      expect(output).to.equal('Must not be one of: word, test, done.');
    });

    it('Given value is a string and the enumeration does not contain that string, but each entry also contains extra data, it should return undefined', () => {
      // Arrange
      const value = 'test';
      const enumeration = [
        {
          key: 'hi',
          value: 'word'
        },
        {
          key: 'bye',
          value: 'nope'
        },
        {
          key: 'last',
          value: 'done'
        }
      ];
      const map = function (entry) {
        return entry.value;
      }

      // Act
      const output = notOneOf(enumeration, map)(value);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given the enumeration is not an array, it should throw an exception', () => {
      // Arrange
      const value = 'test';
      const enumeration = 'string';
      const testFunction = () => {
        notOneOf(enumeration)(value);
      };

      // Act


      // Assert
      expect(testFunction).to.throw('Enumeration must be an array');
    });

  });

  describe ('contains', () => {
    it('Given the value is an integer and is in the provided array, it should return undefined', () => {
      // Arrange
      const value = 5;
      const array = [1, 2, 3, 4, 5];

      // Act
      const output = contains(value)(array);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given the value is an integer and is not in the provided array, it should return an error message', () => {
      // Arrange
      const value = 6;
      const array = [1, 2, 3, 4, 5];

      // Act
      const output = contains(value)(array);

      // Assert
      expect(output).to.equal('Must contain: 6.');
    });

    it('Given the value is a string and is in the provided array, it should return undefined', () => {
      // Arrange
      const value = 'hi';
      const array = ['hi', 'bye', 'why'];

      // Act
      const output = contains(value)(array);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given the value is a string and is not in the provided array, it should return undefined', () => {
      // Arrange
      const value = 'phi';
      const array = ['hi', 'bye', 'why'];

      // Act
      const output = contains(value)(array);

      // Assert
      expect(output).to.equal('Must contain: phi.');
    });

    it('Given the value is null and is in the provided array, it should return undefined', () => {
      // Arrange
      const value = null;
      const array = [1, 2, 3, 4 , null];

      // Act
      const output = contains(value)(array);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given the value is null and is not in the provided array, it should return undefined', () => {
      // Arrange
      const value = null;
      const array = [1, 2, 3, 4 ,5];

      // Act
      const output = contains(value)(array);

      // Assert
      expect(output).to.equal('Must contain: null.');
    });

    it('Given the provided array is not an array, it should throw an exception', () => {
      // Arrange
      const value = 'test';
      const array = 'string';
      const testFunction = () => {
        contains(value)(array);
      };

      // Act


      // Assert
      expect(testFunction).to.throw('Provided array must be an array');
    });

  });

  describe('unique', () => {
    it('Given value is a string and is unique in the collection with respect to its key, it will return undefined', () => {
      // Arrange
      const value = 'Weston';
      const data = {
        name: 'Weston',
        state: 'Florida'
      };
      const key = 'name';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida'
        },
        {
          name: 'Ft. Lauderdale',
          state: 'Florida'
        },
        {
          name: 'Los Angeles',
          state: 'California'
        }
      ];

      // Act
      const output = unique(value, data, key, collection);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a string and is not unique in the collection with respect to its key, it will return an error message', () => {
      // Arrange
      const value = 'Weston';
      const data = {
        name: 'Weston',
        state: 'Florida'
      };
      const key = 'name';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida'
        },
        {
          name: 'Weston',
          state: 'Florida'
        },
        {
          name: 'Los Angeles',
          state: 'California'
        }
      ];

      // Act
      const output = unique(value, data, key, collection);

      // Assert
      expect(output).to.equal('name must be unique. That one is already taken.');
    });

    it('Given value is a number and is unique in the collection with respect to its key, it will return undefined', () => {
      // Arrange
      const value = 42;
      const data = {
        name: 'Weston',
        state: 'Florida',
        population: 42
      };
      const key = 'population';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida',
          population: 106
        },
        {
          name: 'Ft. Lauderdale',
          state: 'Florida',
          population: 97
        },
        {
          name: 'Los Angeles',
          state: 'California',
          population: 235
        }
      ];

      // Act
      const output = unique(value, data, key, collection);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a number and is not unique in the collection with respect to its key, it will return an error message', () => {
      // Arrange
      const value = 42;
      const data = {
        name: 'Weston',
        state: 'Florida',
        population: 42
      };
      const key = 'population';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida',
          population: 106
        },
        {
          name: 'Ft. Lauderdale',
          state: 'Florida',
          population: 42
        },
        {
          name: 'Los Angeles',
          state: 'California',
          population: 235
        }
      ];

      // Act
      const output = unique(value, data, key, collection);

      // Assert
      expect(output).to.equal('population must be unique. That one is already taken.');
    });

  });

  describe('valueExistsInCollection', () => {
    it('Given value is a string and is unique in the collection with respect to its key, it will return an error message', () => {
      // Arrange
      const value = 'Weston';
      const data = {
        name: 'Weston',
        state: 'Florida'
      };
      const key = 'name';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida'
        },
        {
          name: 'Ft. Lauderdale',
          state: 'Florida'
        },
        {
          name: 'Los Angeles',
          state: 'California'
        }
      ];

      // Act
      const output = valueExistsInCollection(value, data, key, collection);

      // Assert
      expect(output).to.equal('name must exist with value Weston.');
    });

    it('Given value is a string and is not unique in the collection with respect to its key, it will return undefined', () => {
      // Arrange
      const value = 'Weston';
      const data = {
        name: 'Weston',
        state: 'Florida'
      };
      const key = 'name';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida'
        },
        {
          name: 'Weston',
          state: 'Florida'
        },
        {
          name: 'Los Angeles',
          state: 'California'
        }
      ];

      // Act
      const output = valueExistsInCollection(value, data, key, collection);

      // Assert
      expect(output).to.equal(undefined);
    });

    it('Given value is a number and is unique in the collection with respect to its key, it will return an erorr message', () => {
      // Arrange
      const value = 42;
      const data = {
        name: 'Weston',
        state: 'Florida',
        population: 42
      };
      const key = 'population';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida',
          population: 106
        },
        {
          name: 'Ft. Lauderdale',
          state: 'Florida',
          population: 97
        },
        {
          name: 'Los Angeles',
          state: 'California',
          population: 235
        }
      ];

      // Act
      const output = valueExistsInCollection(value, data, key, collection);

      // Assert
      expect(output).to.equal('population must exist with value 42.');
    });

    it('Given value is a number and is not unique in the collection with respect to its key, it will return undefined', () => {
      // Arrange
      const value = 42;
      const data = {
        name: 'Weston',
        state: 'Florida',
        population: 42
      };
      const key = 'population';
      const collection = [
        {
          name: 'Miami',
          state: 'Florida',
          population: 106
        },
        {
          name: 'Ft. Lauderdale',
          state: 'Florida',
          population: 42
        },
        {
          name: 'Los Angeles',
          state: 'California',
          population: 235
        }
      ];

      // Act
      const output = valueExistsInCollection(value, data, key, collection);

      // Assert
      expect(output).to.equal(undefined);
    });

  });

  describe('createValidator', () => {
    describe('nested objects', () => {
      it('validations should work with nested objects of arbitrary levels of nesting', () => {
        // Arrange.
        const testValidation = createValidator({
        	name: [required],
        	address: [required],
          email: [required, minLength(15), email],
        	contact: {
        		name: [required],
        		friend: {
        			name: [required],
        			phone: [required]
        		}
        	}
        });

        const testObject = {
        	name: 'bob',
        	address: 'addr',
          email: 'invalid',
        	contact: {
        		name: 'joe',
        		friend: {
        			name: 'sue',
        			phone: null
        		}
        	}
        }

        const expectedObject = {
          email: [
            'Must be at least 15 characters',
            'Invalid email address'
          ],
          contact: {
            friend: {
              phone: ['Required']
            }
          }
        };

        // Act.
        const errors = testValidation(testObject);

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });
    });

    describe('unique', () => {
      it('Non-unique values of a field should fail uniqueness check', () => {
        // Arrange.
        const testValidation = createValidator({
          name: [required],
          address: [required],
          contact: {
            name: [required],
            friend: {
              name: [required, unique],
              phone: [required]
            }
          }
        });

        const testObject = {
          name: 'bob',
          address: 'addr',
          contact: {
            name: 'joe',
            friend: {
              name: 'sue',
              phone: 5
            }
          }
        }

        const testCollection = [
          {
            name: 'mary',
            address: 'addr',
            contact: {
              name: 'joe',
              friend: {
                name: 'sue',
                phone: 5
              }
            }
          },
          {
            name: 'bob',
            address: 'addr',
            contact: {
              name: 'joe',
              friend: {
                name: 'mary',
                phone: 5
              }
            }
          }
        ];

        const expectedObject = {
          contact: {
            friend: {
              name: ['name must be unique. That one is already taken.']
            }
          }
        };


        // Act.
        const errors = testValidation(testObject, null, testCollection);

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });
    });

    describe('valueExistsInCollection', () => {
      it('A field that has a value that does not exist in collection should fail valueExistsInCollection check', () => {
        // Arrange.
        const testValidation = createValidator({
          name: [required, valueExistsInCollection]
        });

        const testObject = {
          name: 'bob',
        }

        const testCollection = [
          {
            name: 'test',
          },
          {
            name: 'edgar',
          }
        ];

        const expectedObject = {
          name: ['name must exist with value bob.']
        };


        // Act.
        const errors = testValidation(testObject, null, testCollection);

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });
    });

    describe('foreign keys', () => {
      it('foreign keys that do not match a primary key should fail foreign key check', () => {
        // Arrange.
        const testValidation = createValidator({
          name: [required],
          friendID: [required],
          school: {
            name: [required],
            principalID: [required],
            county: {
              _id: [required, minLength(15), integer],
              name: [required]
            }
          }
        }, {
          friendID: {
            primary: '_id',
            collection: 'friends'
          },
          school: {
            principalID: {
              primary: '_id',
              collection: 'principals',
            },
            county: {
              _id: {
                primary: '_id',
                collection: 'counties'
              }
            }
          }
        });

        const personObject = {
          name: 'bob',
          friendID: 'mkifgr',
          school: {
            name: 'School X',
            principalID: 'aaaaaa',
            county: {
              _id: '123456a',
              name: 'County A'
            }
          }
        };

        const friendCollection = [
          {
            _id: 'mjufgr',
            name: 'joe'
          },
          {
            _id: 'nhycfv',
            name: 'suzy'
          }
        ];

        const principalCollection = [
          {
            _id: 'aaaaaa',
            name: 'Principal X'
          },
          {
            _id: 'dhsnhg',
            name: 'Principal Y'
          }
        ];

        const countyCollection = [
          {
            _id: 'hndkal',
            name: 'County X'
          },
          {
            _id: 'mlkijh',
            name: 'County Y'
          }
        ];

        const expectedObject = {
          friendID: ['_id must exist with value mkifgr.'],
          school: {
            county: {
              _id: [
                '_id must exist with value 123456a.',
                'Must be at least 15 characters',
                'Must be an integer'
              ]
            }
          }
        };


        // Act.
        const errors = testValidation(personObject, null, {
          friends: friendCollection,
          principals: principalCollection,
          counties: countyCollection
        });

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });

      it('foreign keys that are validated against empty collections should fail foreign key check', () => {
        // Arrange.
        const testValidation = createValidator({
          name: [required],
          friendID: [required],
          school: {
            name: [required],
            principalID: [required],
            county: {
              _id: [required, minLength(15), integer],
              name: [required]
            }
          }
        }, {
          friendID: {
            primary: '_id',
            collection: 'friends'
          },
          school: {
            principalID: {
              primary: '_id',
              collection: 'principals',
            },
            county: {
              _id: {
                primary: '_id',
                collection: 'counties'
              }
            }
          }
        });

        const personObject = {
          name: 'bob',
          friendID: 'mkifgr',
          school: {
            name: 'School X',
            principalID: 'aaaaaa',
            county: {
              _id: '123456a',
              name: 'County A'
            }
          }
        };

        const friendCollection = [];

        const principalCollection = [];

        const countyCollection = [];

        const expectedObject = {
          friendID: ['_id must exist with value mkifgr.'],
          school: {
            principalID: ['_id must exist with value aaaaaa.'],
            county: {
              _id: [
                '_id must exist with value 123456a.',
                'Must be at least 15 characters',
                'Must be an integer'
              ]
            }
          }
        };


        // Act.
        const errors = testValidation(personObject, null, {
          friends: friendCollection,
          principals: principalCollection,
          counties: countyCollection
        });

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });

      it('foreign keys that all match a primary key should pass foreign key check', () => {
        // Arrange.
        const testValidation = createValidator({
          name: [required],
          friendID: [required],
          school: {
            name: [required],
            principalID: [required],
            county: {
              _id: [required],
              name: [required]
            }
          }
        }, {
          friendID: {
            primary: '_id',
            collection: 'friends'
          },
          school: {
            principalID: {
              primary: '_id',
              collection: 'principals',
            },
            county: {
              _id: {
                primary: '_id',
                collection: 'counties'
              }
            }
          }
        });

        const personObject = {
          name: 'bob',
          friendID: 'nhycfv',
          school: {
            name: 'School X',
            principalID: 'aaaaaa',
            county: {
              _id: '123456',
              name: 'County A'
            }
          }
        };

        const friendCollection = [
          {
            _id: 'mjufgr',
            name: 'joe'
          },
          {
            _id: 'nhycfv',
            name: 'suzy'
          }
        ];

        const principalCollection = [
          {
            _id: 'aaaaaa',
            name: 'Principal X'
          },
          {
            _id: 'dhsnhg',
            name: 'Principal Y'
          }
        ];

        const countyCollection = [
          {
            _id: 'hndkal',
            name: 'County X'
          },
          {
            _id: '123456',
            name: 'County Y'
          }
        ];

        const expectedObject = {};


        // Act.
        const errors = testValidation(personObject, null, {
          friends: friendCollection,
          principals: principalCollection,
          counties: countyCollection
        });

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });



      it('optional foreign keys that do not exist should pass foreign key validation', () => {
        // Arrange.
        const testValidation = createValidator({
          name: [required],
          school: {
            name: [required],
            principalID: [required],
            county: {
              _id: [required],
              name: [required]
            }
          }
        }, {
          friendID: {
            primary: '_id',
            collection: 'friends'
          },
          school: {
            principalID: {
              primary: '_id',
              collection: 'principals',
            },
            county: {
              _id: {
                primary: '_id',
                collection: 'counties'
              }
            }
          }
        });

        const personObject = {
          name: 'bob',
          school: {
            name: 'School X',
            principalID: 'aaaaaa',
            county: {
              _id: '123456',
              name: 'County A'
            }
          }
        };

        const friendCollection = [
          {
            _id: 'mjufgr',
            name: 'joe'
          },
          {
            _id: 'nhycfv',
            name: 'suzy'
          }
        ];

        const principalCollection = [
          {
            _id: 'aaaaaa',
            name: 'Principal X'
          },
          {
            _id: 'dhsnhg',
            name: 'Principal Y'
          }
        ];

        const countyCollection = [
          {
            _id: 'hndkal',
            name: 'County X'
          },
          {
            _id: '123456',
            name: 'County Y'
          }
        ]

        const expectedObject = {};


        // Act.
        const errors = testValidation(personObject, null, {
          friends: friendCollection,
          principals: principalCollection,
          counties: countyCollection
        });

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });
    });

    describe('overrideErrorMessage', () => {
      it('Should return the custom message when wrapped with overrideErrorMessage and the value is invalid.', () => {
        // Arrange.
        const testValidation = createValidator({
          name: [required, overrideErrorMessage( minLength(10), 'BOOM' )]
        });

        const testObject = {
          name: 'dogood',
        }

        const expectedObject = {
          name: ['BOOM']
        };


        // Act.
        const errors = testValidation(testObject);

        // Assert.
        expect(errors).to.deep.equal(expectedObject);
      });
    });

  });

});
