# Validations

## Installation

run this command to add this to your `package.json`:

`npm install --save bitbucket:ultilabs/validations#HEAD`

## Usage

```javascript
import { required, minLength, maxLength, unique, isString, createValidator } from 'validations';

const validateProduct = createValidator({
  name: [required, minLength(8)],
  address: [required, maxLength(50)],
  contact: {
    name: [required, unique],
    friend: {
      name: [required, isString],
      phone: [required, integer]
    }
  }
});

const errors = validateProduct(product, null, productAggregate);
```

`errors` mimics the structure of the passed in object.

Let's say that there were a a error for `address`, `contact.name`, and two errors for `contact.friend.phone`. The resulting object would look like so:
```javascript
console.log(errors);
/*
  address: ['Error message']
  contact: {
    name: ['Error message']
    friend: {
      phone: ['Error message one', 'Error message two']
    }
  }
*/
```
Errors are stackable, meaning one key on the errors object can correspond to multiple errors for that key (which are represented as multiple entries in the corresponding array).

### Foreign Key Validations
There is an alternate way in which `createValidator` and the resultant validator function can be called which is useful for foreign key validations.

Example:
```javascript
const validateStudent = createValidator({
  name: [required],
  friendID: [required],
  school: {
    name: [required],
    principalID: [required],
    county: {
      _id: [required],
      name: [required]
    }
  }
}, {
  friendID: {
    primary: '_id',
    collection: 'friends'
  },
  school: {
    principalID: {
      primary: '_id',
      collection: 'principals',
    },
    county: {
      _id: {
        primary: '_id',
        collection: 'counties'
      }
    }
  }
});

const personObject = {
  name: 'bob',
  friendID: 'nhycfv',
  school: {
    name: 'School X',
    principalID: 'aaaaaa',
    county: {
      _id: '123456',
      name: 'County A'
    }
  }
};

const friendCollection = [
  {
    _id: 'mjufgr',
    name: 'joe'
  },
  {
    _id: 'nhycfv',
    name: 'suzy'
  }
];

const principalCollection = [
  {
    _id: 'aaaaaa',
    name: 'Principal X'
  },
  {
    _id: 'dhsnhg',
    name: 'Principal Y'
  }
];

const countyCollection = [
  {
    _id: 'hndkal',
    name: 'County X'
  },
  {
    _id: '123456',
    name: 'County Y'
  }
];

const errors = validateStudent(personObject, null, {
  friends: friendCollection,
  principals: principalCollection,
  counties: countyCollection
});
```
The second parameter to the `createValidator` function maps the foreign keys on the data to be validated to the primary keys they map to and specifies which collections to look in. Accordingly, the third argument to the resultant validation function (`validateStudent` in this case) can now be an object where each key in the object maps to a collection to use for validation. Use the special key `root` to signify the collection that should be used for validations performed on the data object itself (`personObject` in this case). This `root` collection is the same collection that would be passed in as the entire third argument if there were no foreign keys to validate.
